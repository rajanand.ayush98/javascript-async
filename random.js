// Building your own promise based functions. First, solve it using promises. Then, refactor all solutions using async await.

// Task 1: Right now, the function fetchRandomNumbers can be used by passing a callback, Your task is to promisfy this function so that the following can be done:

// fetchRandomNumbers().then((randomNum) => { console.log(randomNum) });

// Similarlly, do the same for the function fetchRandomString

// Task 2: Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable -> add it to the same sum variable and print the sum variable.

// Task 3: Fetch a random number and a random string simultaneously, concatenate their and print the concatenated string

// Task 4: Fetch 10 random numbers simultaneously -> and print their sum.

// function fetchRandomNumbers(callback){
//     console.log('Fetching number...');
//     setTimeout(() => {
//         let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
//         console.log('Received random number:', randomNum);
//         callback(randomNum);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }
function fetchRandomNumbers(){
    return new Promise((resolve, reject)=>{
        console.log('Fetching Number...')
        setTimeout(()=>{
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000)
    })
}

// function fetchRandomString(callback){
//     console.log('Fetching string...');
//     setTimeout(() => {
//         let result           = '';
//         let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//         let charactersLength = characters.length;
//         for ( let i = 0; i < 5; i++ ) {
//            result += characters.charAt(Math.floor(Math.random() * charactersLength));
//         }
//         console.log('Received random string:', result);
//         callback(result);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }

function fetchRandomString(){
    return new Promise((resolve, reject)=>{

        console.log('Fetching string...');
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

/** task 2 */
var sum = 0
fetchRandomNumbers().then((randomNum) => {
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum)
})

/** task 3 */
var output
fetchRandomNumbers().then((randomNum) =>{
    output = randomNum
    return fetchRandomString()
}).then((str)=>{
    console.log(output+" "+ str)
})
// console.log(sum)

// fetchRandomNumbers().then((randomNum) => console.log(randomNum))
// fetchRandomString().then((randomStr) => console.log(randomStr))

/** task 4 */

fetchRandomNumbers().then((randomNum) => {
    sum = 0
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum) => {
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum) => {
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum) => {
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum) => {
    sum += randomNum
    console.log(sum)
    return fetchRandomNumbers()
}).then((randomNum)=>{
    sum += randomNum
    console.log(sum + ' final sum')
})